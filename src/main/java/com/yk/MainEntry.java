package com.yk;

import com.yk.mySpring.core.annotation.ComponentScan;
import com.yk.mySpring.core.superInter.impl.ApplicationContext;

@ComponentScan("com.yk")
public class MainEntry {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ApplicationContext(MainEntry.class);
        applicationContext.run();
    }
}
