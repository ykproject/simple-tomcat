package com.yk.mySpring.core.superInter.impl;

import com.yk.mySpring.core.annotation.ComponentScan;
import com.yk.mySpring.core.superInter.BeanFactory;
import com.yk.myTomcat.annotation.Servlet;
import com.yk.myTomcat.context.ServletContext;
import com.yk.myTomcat.server.TomcatServer;
import com.yk.myTomcat.servlet.HttpServlet;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ApplicationContext implements BeanFactory {
    private final Class configClass;
    private Map<String,String> propertiesMap = new HashMap<>();

    public ApplicationContext(Class  configClass) {
        this.configClass = configClass;
        try {
            initPropetires();
            scanServletBeans();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initPropetires() {
        try {
            String[] split;
            URL resource = this.configClass.getClassLoader().getResource("application.properties");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(new FileInputStream(new File(resource.getFile()))));
            String strProperties = "";
            while ((strProperties = bufferedReader.readLine()) != null){
                if(strProperties == null){
                    break;
                }
                split = strProperties.split("=");
                if(split.length>1){
                    propertiesMap.put(split[0].toUpperCase(),split[1]);
                }
            }

            Field[] fields = ServletContext.class.getDeclaredFields();
            for (Field field : fields) {
                if(propertiesMap.get(field.getName()) != null){
                    field.setAccessible(true);
                    field.set(ServletContext.class,propertiesMap.get(field.getName()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scanServletBeans() throws Exception {
        ClassLoader classLoader = this.configClass.getClassLoader();
        boolean b = this.configClass.isAnnotationPresent(ComponentScan.class);
        if(b){
            ComponentScan annotation = (ComponentScan) this.configClass.getAnnotation(ComponentScan.class);
            String scanPath = annotation.value();
            scanPath = scanPath.replace(".","/");
            URL url = classLoader.getResource(scanPath);
            File file = new File(url.getFile());
            List<File> classFiles = new ArrayList<>();
            if(file.isDirectory()){
                getClassFiles(file,classFiles);
            }
            //file.getAbsolutePath() -> D:\IDEAlib\mianshi\2022\SimpleTomcat\target\classes\com\yk\MainConfig.class
            List<String> loadClassPath =
                    classFiles.stream()
                            .map(f -> f.getAbsolutePath().replace("\\",".")
                                    .split(".classes.")[1].
                                    replace(".class",""))
                                        .collect(Collectors.toList());
            for (String s : loadClassPath) {
                Class<?> loadClass = classLoader.loadClass(s);
                boolean isServlet = loadClass.isAnnotationPresent(Servlet.class);
                if(isServlet){
                    String[] servletUrl = loadClass.getAnnotation(Servlet.class).value();
                    for (String sUrl : servletUrl) {
                        ServletContext.setServletMapping(sUrl,(HttpServlet) loadClass.newInstance());
                    }
                }
            }

//            loadClassPath.forEach((f)->{
//                System.out.println(f);
//            });
//            System.out.println("url.getFile()::"+url.getFile());
        }else {
            System.out.println("!!!ApplicationContext--scanBeans--No Search ComponentScan");
        }
    }

    /**
     *拿到所有.class文件
     */
    public void getClassFiles(File file , List<File> classFiles){
        if(file.isDirectory()){
            for (File listFile : file.listFiles()) {
                getClassFiles(listFile,classFiles);
            }
        }else {
            classFiles.add(file);
        }
    }

    public void run() {
        TomcatServer tomcatServer = new TomcatServer();
        tomcatServer.start();
    }
}
