package com.yk.myTomcat.http;

import com.yk.myTomcat.context.HttpContext;

import java.io.*;
import java.net.Socket;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import com.alibaba.fastjson.*;
import com.yk.myTomcat.context.ServletContext;

import javax.annotation.Resource;

public class HttpResponse {
    private Socket socket;
    private OutputStream os;
    private Map<String,String> headers = new HashMap<>();
    private File file;
    private byte[] data;
    private Integer statusCode = 1;

    public HttpResponse(Socket socket) {
        try {
            this.socket = socket;
            this.os = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void flush() {
        try{
            sendResponseLine();
            sendHeader();
            sendContent();
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    //响应体
    private void sendContent() throws IOException {
        if(this.file != null && this.data == null ){
            String[] split = this.file.getName().split("\\.");
            setContentType(HttpContext.getContentTypeByFileName(split[split.length-1]));
            setContentLength(Integer.valueOf(this.file.length()+""));

            FileInputStream fis = new FileInputStream(this.file);
            //缓冲区
            byte[] buff = new byte[1024*10];
            int length = -1;
            while ((length = fis.read(buff)) != -1){
                this.os.write(buff,0,length);
            }
        }else if(this.data != null && this.file == null && this.statusCode < 400){
            os.write(data);
        }else {
            if(this.statusCode != 302){
                System.out.println("HttpResponse->sendContent:No Data Or File To Response!");
            }
        }
    }



    public void sendJSON(Object o)   {
        this.statusCode = 200;
        if(o instanceof String ||
           o instanceof Integer ||
           o instanceof Double ||
           o instanceof Float ||
           o instanceof Boolean ||
           o instanceof Character ){
            StringBuilder builder = new StringBuilder();
            builder.append("<html>");
            builder.append("<head>");
            builder.append("<meta charset=\"utf-8\">");
            builder.append("<title></title>");
            builder.append("</head>");
            builder.append("<body>");
            builder.append("<h1>"+String.valueOf(o)+"</h1>");
            builder.append("</body>");
            builder.append("</html>");

            setContentType("text/html");
            setContentLength(builder.length());
            try {
                this.data = builder.toString().trim().getBytes("utf-8");
                flush();
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            setContentType("application/json");
            this.data = JSON.toJSONString(o).getBytes();
            flush();
        }

    }

    public void sendJSON(Object o,Integer statusCode){
        if(o instanceof String ||
                o instanceof Integer ||
                o instanceof Double ||
                o instanceof Float ||
                o instanceof Boolean ||
                o instanceof Character ){
            this.data = String.valueOf(o).getBytes();

        }else {
            this.data = JSON.toJSONString(o).getBytes();
        }
        this.statusCode = statusCode;
        setContentType("application/json");
        flush();
    }

    public void sendHtml(String htmlPath){
        this.statusCode = 200;
        this.file = new File(ServletContext.VIEWPERFIX +htmlPath);
        if(this.file == null){
            this.file = new File(ServletContext.VIEWPERFIX + "404.html");
        }
        setContentType("text/html");
        flush();
    }

    public void sendHtml(String htmlPath,Integer statusCode){
        this.statusCode = statusCode;
        this.file = new File(ServletContext.VIEWPERFIX +htmlPath);
        if(this.file == null){
            this.file = new File(ServletContext.VIEWPERFIX + "404.html");
        }
        setContentType("text/html");
        flush();
    }

    //响应状态
    public void sendResponseLine() throws IOException {
        String line
                = "HTTP/1.1 " + statusCode + " " + HttpContext.getStatusInfo(statusCode);
        print(line);
    }

    //响应头
    private void sendHeader() throws IOException {
        Set<Map.Entry<String, String>> entries = this.headers.entrySet();
        for(Map.Entry<String,String> entry : entries ){
            print(entry.getKey() + ": " +entry.getValue());
        }
        //分割响应头跟返回的数据
        print("");
    }

    public void print(String line) throws IOException {
        if(null != this.os){
            os.write(line.getBytes(StandardCharsets.ISO_8859_1));
            os.write(HttpContext.CR);
            os.write(HttpContext.LF);
        }
    }

    public void setHeaders(String key,String value) {
        this.headers.put(key,value);
    }

    public void setFile(File file) {
        this.file = file;
        this.data = null;
    }

    public void setData(byte[] data) {
        this.data = data;
        this.file = null;
    }

    public void setContentType(String contentType) {
        this.headers.put("Content-Type",contentType);
    }

    public void setContentLength(Integer contentLength) {
        this.headers.put("Content-Length",contentLength+"");
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

}
