package com.yk.myTomcat.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class HttpRequest {

    private Socket socket;
    private InputStream is;
    private String requestURI;
    private Map<String,String> headers = new HashMap<String,String>();
    private Map<String,String> parameters = new HashMap<String,String>();
    private String method;
    private String protocol;
    private String queryString;
    private String requestBody;

    private List<String> requestInfo = new ArrayList<>();

    public HttpRequest(Socket socket) {
        this.socket = socket;
        parseInfo();
    }

    /**
     * 解析请求信息 赋值给request
     */
    private void parseInfo() {
        try {
            is = this.socket.getInputStream();
            getHeaderInfo();
//            this.requestInfo.forEach(System.out::println);
            parseRequestLine();
            parseHeader();
            parseContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 1.按照行 拿到请求信息 保存到requestInfo
     * @return
     */
    private void getHeaderInfo() {
        try {
            StringBuilder builder = new StringBuilder();
            InputStreamReader isr = new InputStreamReader(is);
            char[] chars = new char[1024];
            int len;
            while ((len = isr.read(chars)) != -1){
                builder.append(chars,0,len);
                if(len < chars.length){
                    break;
                }
            }
            this.requestInfo
                    .addAll(Arrays.asList(new String(builder.toString().getBytes(StandardCharsets.UTF_8))
                    .split("\r\n")));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 2.根据请求信息解析请求头
     */
    private void parseRequestLine() {
        if(this.requestInfo.size()>0){
            //按照空格拆分  GET / HTTP/1.1
            if(!this.requestInfo.get(0).contains("HTTP/1.1")){
                return;
            }
            String[] head = this.requestInfo.get(0).split("\\s");
            System.out.println("null??"+this.requestInfo.get(0));
            if(head.length == 3){
                this.method = head[0];
                this.requestURI = head[1];
                this.protocol = head[2];
                if("GET".equals(this.method)){
                    if(this.requestURI.indexOf("?")>0){
                        String[] split = this.requestURI.split("\\?");
                        this.requestURI = split[0];
                        this.queryString = split[1];
                        parseQueryString();
                    }
                }
                if("POST".equals(this.method)){
                    this.requestBody = this.requestInfo.get(this.requestInfo.size()-1);
                    this.queryString = this.requestBody;
                }
            }
        }
    }

    /**
     * 3.解析请求头 并赋值
     */
    private void parseHeader() {
        for (int i = 1; i < this.requestInfo.size(); i++) {
            String line = requestInfo.get(i);
            //Host: localhost:8123
            String[] lines = line.split(": ");
            if (lines.length>1){
                this.headers.put(lines[0].trim(),lines[1].trim());
            }
        }
    }

    /**
     * 4.请求头有Content-Length 解析请求body
     */
    private void parseContent() {
        if(headers.containsKey("Content-Length") && "POST".equals(this.method)){
            if("application/x-www-form-urlencoded".equals(headers.get("Content-Type"))){
                parseQueryString();
            }
        }
    }

    /**
     * 5.解析请求参数 parameters
     */
    private void parseQueryString() {
        if(this.queryString != null){
            //id=1&name=2
            String[] params = this.queryString.split("&");
            for (int i = 0; i < params.length; i++) {
                String[] parmKV = params[i].split("=");
                if(parmKV.length>1){
                    String key = URLDecoder.decode(parmKV[0]);
                    String val = URLDecoder.decode(parmKV[1]);
                    this.parameters.put(key,val);
                }
            }

        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public String getRequestURI() {
        return requestURI;
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    public String getHeaders(String key) {
        return headers.get(key);
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getParameters(String key) {
        return parameters.get(key);
    }

    public Set<String> getParameterNames() {
        return parameters.keySet();
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getRequestBody() {
        return requestBody;
    }

    @Override
    public String toString() {
        return "HttpRequest{" +
                "socket=" + socket +
                ", is=" + is +
                ", requestURI='" + requestURI + '\'' +
                ", headers=" + headers +
                ", parameters=" + parameters +
                ", method='" + method + '\'' +
                ", protocol='" + protocol + '\'' +
                ", queryString='" + queryString + '\'' +
                ", requestInfo=" + requestInfo +
                '}';
    }
}
