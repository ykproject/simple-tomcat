package com.yk.myTomcat.servlet;

import com.yk.myTomcat.context.HttpContext;
import com.yk.myTomcat.context.ServletContext;
import com.yk.myTomcat.http.HttpRequest;
import com.yk.myTomcat.http.HttpResponse;

import java.util.Locale;

public abstract class HttpServlet {
    public void service(HttpRequest request, HttpResponse response) {
        if("GET".equals(request.getMethod().toUpperCase(Locale.ROOT))){
            doGet(request,response);
        }else {
            doPost(request,response);
        }
    }

    public void forward(String url,HttpRequest request, HttpResponse response){
        ServletContext.getServlet(url).service(request,response);
    }

    public void redirect(String url,HttpRequest request, HttpResponse response){
        response.setStatusCode(302);
        response.setHeaders("Location",url);
        //非必须
        response.setHeaders("Content-Language"," zh-CN");
        response.setHeaders("Content-Length", " 0");
        response.setHeaders("Keep-Alive"," timeout=60");
        response.setHeaders("Connection"," keep-alive");
        try {
            response.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract void doGet(HttpRequest request,HttpResponse response);

    public abstract void doPost(HttpRequest request,HttpResponse response);
}
