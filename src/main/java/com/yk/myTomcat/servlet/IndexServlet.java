package com.yk.myTomcat.servlet;

import com.yk.myTomcat.annotation.Servlet;
import com.yk.myTomcat.http.HttpRequest;
import com.yk.myTomcat.http.HttpResponse;
import com.yk.myTomcat.servlet.HttpServlet;

@Servlet({"/","/index","/index.html"})
public class IndexServlet extends HttpServlet {

    @Override
    public void service(HttpRequest request, HttpResponse response) {
        response.sendHtml("index.html");
    }

    @Override
    public void doGet(HttpRequest request, HttpResponse response) {

    }

    @Override
    public void doPost(HttpRequest request, HttpResponse response) {

    }
}
