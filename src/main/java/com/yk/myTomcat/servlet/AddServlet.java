package com.yk.myTomcat.servlet;

import com.yk.mySpring.bean.Person;
import com.yk.myTomcat.annotation.Servlet;
import com.yk.myTomcat.http.HttpRequest;
import com.yk.myTomcat.http.HttpResponse;

import java.nio.charset.StandardCharsets;

@Servlet({"/addPer","/retJson"})
public class AddServlet extends HttpServlet {


    @Override
    public void doGet(HttpRequest request, HttpResponse response) {
        response.sendJSON("GET"+request.getParameters("name")+"///"+request.getParameters("age"));
//        response.setData(("GET张飒///"+request.getParameters("age")).getBytes(StandardCharsets.UTF_8));
//        response.setStatusCode(200);
//        response.flush();
    }

    @Override
    public void doPost(HttpRequest request, HttpResponse response) {
        Person person = new Person();
        person.setName(request.getParameters("name"));
        person.setAge(request.getParameters("age"));
        response.sendJSON(person);
//        response.setData(("POST张飒///"+request.getParameters("age")).getBytes(StandardCharsets.UTF_8));
//        response.setStatusCode(200);
//        response.flush();

    }
}
