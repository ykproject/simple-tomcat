package com.yk.myTomcat.servlet;

import com.yk.myTomcat.http.HttpRequest;
import com.yk.myTomcat.http.HttpResponse;
import com.yk.myTomcat.annotation.*;

import java.io.File;
import java.nio.charset.StandardCharsets;

@Servlet("/hello")
public class HelloServlet extends HttpServlet{

    @Override
    public void doGet(HttpRequest request, HttpResponse response) {
        response.setStatusCode(200);

        //Content-Type: application/json
        response.setContentType("application/json");
//        response.setContentType("text/html");
//        response.setFile(new File("./src/main/webapp/WEB-INF/index.html"));
        response.setHeaders("Connection","keep-alive");
        response.setData("{\"timestamp\":\"2022-04-17T02:40:22.183+00:00\",\"status\":404,\"error\":\"Not Found\",\"path\":\"/favicon.ico\"}".getBytes(StandardCharsets.UTF_8));
        response.flush();
    }

    @Override
    public void doPost(HttpRequest request, HttpResponse response) {

    }
}
