package com.yk.myTomcat.context;

import com.yk.myTomcat.servlet.HttpServlet;

import java.util.HashMap;
import java.util.Map;

public class ServletContext {

    private static Map<String, HttpServlet> SERVLET_MAPPING = new HashMap<>();

    public static String VIEWPERFIX = "";

    public static void setViewPerfix(String viewPerfix) {
        ServletContext.VIEWPERFIX = viewPerfix;
    }

    public static HttpServlet getServlet(String url) {
        return SERVLET_MAPPING.get(url);
    }

    public static void setServletMapping(String url, HttpServlet servlet) {
        SERVLET_MAPPING.put(url,servlet);
    }

}
