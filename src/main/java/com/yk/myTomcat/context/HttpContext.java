package com.yk.myTomcat.context;

import com.yk.mySpring.core.annotation.Component;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HttpContext {

    public static final int CR = 13;
    public static final int LF = 10;
    public static Map<Integer,String> STATUS_INFO = new HashMap<>();
    public static Map<String,String> CONTENT_TYPE_MAP = new HashMap<>();

    static {
        INIT_STATUS_INFO();
        INIT_CONTENT_TYPE_MAP();
    }

    private static void INIT_CONTENT_TYPE_MAP() {
        try {
            ClassLoader classLoader = HttpContext.class.getClassLoader();
            //D:/IDEAlib/mianshi/2022/SimpleTomcat/target/classes
            // 【app类加载器】 相对这个目录下get文件或路径
            URL url = classLoader.getResource("web.xml");

            SAXReader reader = new SAXReader();
            Document document = reader.read(new File(url.getFile()));
            Element root = document.getRootElement();
            List<Element> elements = root.elements();
            elements.forEach( e-> {
                String key = e.elementText("extension");
                String value = e.elementText("mime-type");
                CONTENT_TYPE_MAP.put(key,value);
            });
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private static void INIT_STATUS_INFO() {
        STATUS_INFO.put(1,"");
        STATUS_INFO.put(200,"OK");
        STATUS_INFO.put(302,"Move Temporarily");
        STATUS_INFO.put(404,"Not Found");
        STATUS_INFO.put(500,"Internal Server Error");
    }

    public static String getStatusInfo(Integer statusCode) {
        return STATUS_INFO.get(statusCode);
    }

    public static String getContentTypeByFileName(String filename){
        return CONTENT_TYPE_MAP.get(filename);
    }



}
