package com.yk.myTomcat.server;

import com.yk.myTomcat.annotation.Servlet;
import com.yk.myTomcat.context.HttpContext;
import com.yk.myTomcat.context.ServletContext;
import com.yk.myTomcat.http.HttpRequest;
import com.yk.myTomcat.http.HttpResponse;
import com.yk.myTomcat.servlet.HttpServlet;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class AppClient implements Runnable{
    private Socket socket;
    private InputStream is;
    private OutputStream os;

    public AppClient(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try{
            /*is = socket.getInputStream();
            readStream(is);*/
            HttpRequest request = new HttpRequest(socket);
            HttpResponse response = new HttpResponse(socket);
            HttpServlet servlet = ServletContext.getServlet(request.getRequestURI());
            if(null != servlet){
                servlet.service(request,response);
            }else {
                File file = new File(ServletContext.VIEWPERFIX + request.getRequestURI());
                if(null != file && file.isFile()){
                    response.setStatusCode(200);
                    response.setFile(file);
                    response.flush();
                }else {
                    response.sendHtml("404.html",404);
                }

            }
            if(null != socket && !socket.isClosed()){
                System.out.println("1111socket.close();");
                    socket.close();
            }
//            os = socket.getOutputStream();
//            System.out.println("request:"+request);
//            os.write(("uri:"+request.getRequestURI()+"\n").getBytes(StandardCharsets.ISO_8859_1));
//            os.write(("method:"+request.getMethod()+"\n").getBytes(StandardCharsets.ISO_8859_1));
//            os.write(("paramNames:"+request.getParameterNames()+"\n").getBytes(StandardCharsets.ISO_8859_1));
//            os.close();

            /*
            StringBuilder builder = new StringBuilder();
            builder.append("<html>");
            builder.append("<head>");
            builder.append("<meta charset=\"utf-8\">");
            builder.append("<title>index</title>");
            builder.append("</head>");
            builder.append("<body>");
            builder.append("<h1>hello tomcat</h1>");
            builder.append("</body>");
            builder.append("</html>");

//            os.write(("HTTP/1.1"+" "+200+" OK").getBytes(StandardCharsets.ISO_8859_1));
            println("HTTP/1.1"+" "+200+" OK");


            byte[] bytes = builder.toString().getBytes(StandardCharsets.ISO_8859_1);
            *//*os.write("Content-Type:text/html".getBytes(StandardCharsets.ISO_8859_1));
            os.write(("Content-Length:"+bytes.length).getBytes(StandardCharsets.ISO_8859_1));
            os.write("".getBytes(StandardCharsets.ISO_8859_1));
            os.write(13);//CR
            os.write(10);//LF*//*

            println("Content-Type:text/html");
            println("Content-Length:"+bytes.length);
            println("");

            os.write(bytes);



            socket.close();

            */
        }catch (Exception ex){
            ex.printStackTrace();
        }/*finally {
            if(null != socket && !socket.isClosed()){
                try {
                    socket.close();
                    System.out.println("2222socket.close();");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
    }

    private void readStream( InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        try {
            String s = reader.readLine();
            System.out.println("header:"+s);
            while (!"".equals((s = reader.readLine()))){
                System.out.println("reader:"+s);
                if("".equals(s)){
                    break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void println(String line) {
        try {
            os.write(line.getBytes("ISO8859-1"));
            os.write(13);//CR
            os.write(10);//LF
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
