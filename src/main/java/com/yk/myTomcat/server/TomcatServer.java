package com.yk.myTomcat.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TomcatServer {

    private ServerSocket serverSocket;
    private ExecutorService executorService;

    public TomcatServer() {
        try {
            this.executorService = Executors.newFixedThreadPool(20);
            this.serverSocket = new ServerSocket(8123);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        System.out.println("---服务器启动---");
        try{
            while (true){
                System.out.println("---等待连接---");
                Socket socket = serverSocket.accept();
                AppClient appClient = new AppClient(socket);
                executorService.execute(appClient);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void wtiteMsg(OutputStream os) {
        try {
            os.write(11);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public  void main1(String[] args) throws Exception {


//        File file =
//                new File(System.getProperty("user.dir")+"/src/main/webapp/WEB-INF/web.xml");
//        System.out.println(file.isFile());

//        ClassLoader classLoader = tomcatServer.getClass().getClassLoader();
//        //D:/IDEAlib/mianshi/2022/SimpleTomcat/target/classes 【app类加载器】 相对这个目录下拿文件或路径
//        URL url = classLoader.getResource("application.properties");
//        String file1 = url.getFile();
//        System.out.println(file1);
//        File file = new File(file1);
//        FileInputStream inputStream = new FileInputStream(file);
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//        System.out.println(bufferedReader.readLine());

    }


}
